// Importação das models existentes e framework sequelize
const { response } = require('express');
const User = require('../models/User');
const Title = require('../models/Title');
const Comment = require('../models/Comment');

// Criação da Rota que retorna todos os títulos do banco de dados
const index = async(req,res) => {
    try {
        const titles = await Title.findAll();
        return res.status(200).json({titles});
    }catch(err){
        return res.status(500).json({err});
    }
};

// Criação da Rota que retorna um único título específico do banco de dados
const show = async(req,res) => {
    const {id} = req.params;
    try {
        const title = await Title.findByPk(id);
        return res.status(200).json({title});
    }catch(err){
        return res.status(500).json({err});
    }
};

// Criação da Rota que cria novos títulos no banco de dados
const create = async(req,res) => {
    try{
          const title = await Title.create(req.body);
          return res.status(201).json({message: "Título postado com sucesso!", title: title});
      }catch(err){
          res.status(500).json({error: err});
      }
};

// Criação da Rota que atualiza atributos de um título do banco de dados
const update = async(req,res) => {
    const {id} = req.params;
    try {
        const [updated] = await Title.update(req.body, {where: {id: id}});
        if(updated) {
            const title = await Title.findByPk(id);
            return res.status(200).send(title);
        } 
        throw new Error();
    }catch(err){
        return res.status(500).json("Título não encontrado");
    }
};

// Criação da Rota que deleta um título específico do banco de dados
const destroy = async(req,res) => {
    const {id} = req.params;
    try {
        const deleted = await Title.destroy({where: {id: id}});
        if(deleted) {
            return res.status(200).json("Título deletado com sucesso.");
        }
        throw new Error ();
    }catch(err){
        return res.status(500).json("Título não encontrado.");
    }
};


// Exportação da CRUD criada acima para routes
module.exports = {
    index,
    show,
    create,
    update,
    destroy
};
