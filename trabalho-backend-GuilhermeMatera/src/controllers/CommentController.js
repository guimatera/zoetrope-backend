// Importação das models existentes e framework sequelize
const { response } = require('express');
const User = require('../models/User');
const Title = require('../models/Title');
const Comment = require('../models/Comment');

// Criação da Rota que retorna todos os comentários do banco de dados
const index = async(req,res) => {
    try {
        const comments = await Comment.findAll();
        return res.status(200).json({comments});
    }catch(err){
        return res.status(500).json({err});
    }
};

// Criação da Rota que retorna um único comentário específico do banco de dados
const show = async(req,res) => {
    const {id} = req.params;
    try {
        const comment = await Comment.findByPk(id);
        return res.status(200).json({comment});
    }catch(err){
        return res.status(500).json({err});
    }
};

// Criação da Rota que cria novos comentários no banco de dados
const create = async(req,res) => {
    try{
          const comment = await Comment.create(req.body);
          return res.status(201).json({message: "Comentário cadastrado com sucesso!", comment: comment});
      }catch(err){
          res.status(500).json({error: err});
      }
};

// Criação da Rota que atualiza atributos de um comentário do banco de dados
const update = async(req,res) => {
    const {id} = req.params;
    try {
        const [updated] = await Comment.update(req.body, {where: {id: id}});
        if(updated) {
            const comment = await Comment.findByPk(id);
            return res.status(200).send(comment);
        } 
        throw new Error();
    }catch(err){
        return res.status(500).json("Comentário não encontrado");
    }
};

// Criação da Rota que deleta um comentário específico do banco de dados
const destroy = async(req,res) => {
    const {id} = req.params;
    try {
        const deleted = await Comment.destroy({where: {id: id}});
        if(deleted) {
            return res.status(200).json("Comentário deletado com sucesso.");
        }
        throw new Error ();
    }catch(err){
        return res.status(500).json("Comentário não encontrado.");
    }
};

// Criação da Rota que adiciona relacionamento de um comentário com um usuário
const addRelationUser = async(req,res) => {
    const {id} = req.params;
    try {
        const comment = await Comment.findByPk(id);
        const user = await User.findByPk(req.body.UserId);
        await comment.setUser(user);
        return res.status(200).json(comment);
    }catch(err){
        return res.status(500).json({err});
    }
};

// Criação da Rota que remove relacionamento de um comentário com um usuário
const removeRelationUser = async(req,res) => {
    const {id} = req.params;
    try {
        const comment = await Comment.findByPk(id);
        await comment.setUser(null);
        return res.status(200).json(comment);
    }catch(err){
        return res.status(500).json({err});
    }
};

// Criação da Rota que adiciona relacionamento de um comentário com um tíyulo
const addRelationTitle = async(req,res) => {
    const {id} = req.params;
    try {
        const comment = await Comment.findByPk(id);
        const title = await Title.findByPk(req.body.TitleId);
        await comment.setTitle(title);
        return res.status(200).json(comment);
    }catch(err){
        return res.status(500).json({err});
    }
};

// Criação da Rota que remove relacionamento de um comentário com um título
const removeRelationTitle = async(req,res) => {
    const {id} = req.params;
    try {
        const comment = await Comment.findByPk(id);
        await comment.setTitle(null);
        return res.status(200).json(comment);
    }catch(err){
        return res.status(500).json({err});
    }
};

// Exportação da CRUD e relacionamentos criados acima para routes
module.exports = {
    index,
    show,
    create,
    update,
    destroy,
    addRelationUser,
    removeRelationUser,
    addRelationTitle,
    removeRelationTitle
};